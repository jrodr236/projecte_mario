﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TresorsModel.Models;

namespace Tresors.Controllers
{
    public class NivellControler : ApiController
    {
        Nivell nivel = new Nivell();
        [HttpGet]
        public IHttpActionResult Get()
        {
            return Ok(nivel.llistarNivells());
        }
        [HttpGet]
        public IHttpActionResult nivell(int id)
        {
            Nivell niv = nivel.consultarNivell(id);
            return Ok(niv);
        }
        [HttpPost]
        public IHttpActionResult agregar(Nivell nivel)
        {
            nivel.InsertarNivell(nivel);
            return Ok();
        }
        [HttpPost]
        public IHttpActionResult modificar(Nivell nivel)
        {
            var value=nivel.actualitzarNivell(nivel);
            return Ok(value);
        }
        [HttpGet]
        public IHttpActionResult eliminar(int id)
        {
            var value=nivel.eliminarNivell(id);
            return Ok(value);
        }
    }
}