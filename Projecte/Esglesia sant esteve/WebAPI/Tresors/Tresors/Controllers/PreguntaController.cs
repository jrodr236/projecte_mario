﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using TresorsModel.Models;

namespace Tresors.Controllers
{

    [EnableCors(origins: "http://localhost:20312/Api/Pregunta/", headers: "*", methods: "*")]
    public class PreguntaController : ApiController
    {

        Pregunta pregunta = new Pregunta();

        // GET: api/Pregunta
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Pregunta/5
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [HttpPost]
        public IEnumerable<Pregunta> Get(int id)
        {
            //Pregunta quest = pregunta.consultarPregunta(id);
            IEnumerable<Pregunta> llistaPreguntes = pregunta.llistarPreguntes(id);
            if (llistaPreguntes == null)
            {
                NotFound();
            }

            return llistaPreguntes;
        }

        // POST: api/Pregunta
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Pregunta/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Pregunta/5
        public void Delete(int id)
        {
        }
    }
}