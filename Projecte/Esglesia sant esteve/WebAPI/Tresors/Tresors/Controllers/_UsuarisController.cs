﻿using TresorsModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Usuari.Controllers
{
    [EnableCors(origins: "http://localhost:20312/Api/Usuaris/", headers: "*", methods: "*")]
    public class UsuarisController : ApiController
    {
        Usuaris users = new Usuaris();
        //Consultar: OK
        [HttpGet]
        public IHttpActionResult Get()
        {
            return Ok(users.llistarUsuari());
        }



        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [HttpPost]
        public IHttpActionResult GetUuid(String id)
        {
            Usuaris usuari = users.consultarUuid(id);
            if(usuari == null)
            {
                NotFound();
            }
            return Ok(usuari);
        }

        /*
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [HttpPost]
        public IHttpActionResult Details(String id)
        {
            Usuaris usuari = users.consultarUuid(id);
            if (usuari == null)
            {
                NotFound();
            }
            return Ok(usuari);
        }*/

        //Validar: Nomes GET 
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [HttpPost]
        public IHttpActionResult validar(String nick, String pass)
        {
            Usuaris valor = users.consultaContrassenya(pass, nick);
            if(valor == null)
            {
                return NotFound();
            }
            return Ok(valor);
        }
        //Agregar: OK
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [HttpPost]
        public IHttpActionResult agregar(String nick, String nom, String cognom, String email, String contrassenya)
        {
            Usuaris nouUsuari = new Usuaris();
            nouUsuari.nick = nick;
            nouUsuari.nom = nom;
            nouUsuari.cognom = cognom;
            nouUsuari.email = email;
            nouUsuari.contrassenya = contrassenya;
            nouUsuari.nivell = 1;
            users.InsertarUsuari(nouUsuari);
            return Ok();
        }
        //Modificar: 
        [HttpPost]
        public IHttpActionResult PutUsuari(Usuaris usuari)
        {
            users.actualitzarUsuari(usuari);
            return Ok();
        }


        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [HttpPost]
        public IHttpActionResult UpdateNivell(String id)
        {
            Usuaris consulta = users.consultarUuid(id);
            consulta.nivell = consulta.nivell + 1;
            consulta.actualitzarUsuari(consulta);
            return Ok(consulta);
        }


        //Eliminar: OK
        [HttpGet]
        public IHttpActionResult Delete(String id)
        {
            users.eliminarUsuari(id);
            return Ok();
        }
    }
}
