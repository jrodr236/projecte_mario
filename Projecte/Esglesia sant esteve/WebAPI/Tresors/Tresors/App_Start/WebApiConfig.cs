﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;

namespace Tresors
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            config.EnableCors();
            // Web API routes
            config.MapHttpAttributeRoutes();
            

           // config.Routes.MapHttpRoute("DefaultApiWithId", "api/{controller}/{id}", new { id = RouteParameter.Optional }, new { id = @"\d+" });

           // config.Routes.MapHttpRoute("DefaultApiWithIdAndPassword", "api/{controller}/{id}", new { id = RouteParameter.Optional }, new { id = @"\d+" });

            config.Routes.MapHttpRoute(
                name: "ActionApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional });

            config.Routes.MapHttpRoute(
                name: "ActionApi2",
                routeTemplate: "api/{controller}/{action}/{nick}/{pass}",
                defaults: new { nick = RouteParameter.Optional, pass = RouteParameter.Optional });

            config.Routes.MapHttpRoute(
                name: "ActionApi3",
                routeTemplate: "api/{controller}/{action}/{nick}/{nom}/{cognom}/{email}/{contrassenya}",
                defaults: new { nick = RouteParameter.Optional, nom = RouteParameter.Optional, cognom = RouteParameter.Optional, email = RouteParameter.Optional, contrassenya = RouteParameter.Optional });

            
            /*  config.MapHttpAttributeRoutes();
              config.Routes.MapHttpRoute("DefaultApiWithId", "api/{controller}/{id}", new { id = RouteParameter.Optional }, new { id = @"\d+" });
              config.Routes.MapHttpRoute("DefaultApiWithAction", "api/{controller}/{action}/{id}", new { id = RouteParameter.Optional });
              config.Routes.MapHttpRoute("DefaultApiGet", "api/{controller}", new { action = "Get" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });
              config.Routes.MapHttpRoute("DefaultApiPost", "api/{controller}", new { action = "Post" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });
              */

        }
    }
}
