﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TresorsModel.Models;
using System.Threading.Tasks;

namespace UnitTest_Model
{
    [TestClass]
    public class UnitTest_Model
    {
        [TestMethod]
        public void TestGeneral()
        {
            Usuaris usuariClass = new Usuaris();
            usuariClass.nick = "provanick";
            usuariClass.nom = "provanom";
            usuariClass.contrassenya = "holis";

            Usuaris usuariClass2 = new Usuaris();
            usuariClass2.nick = "hola";
            usuariClass2.nom = "HolaNom";
            usuariClass2.contrassenya = "adeu";

            insert(usuariClass, usuariClass2);
            list(usuariClass);
            consultaUsuari(usuariClass);
            consultaContrassenya(usuariClass.contrassenya, usuariClass.nick);
            actualitza(usuariClass2);
            elimina(usuariClass);
   
        }
        [TestMethod]
        public void insert(Usuaris usuariClass, Usuaris usuariClass2)
        {
            //INSERTAR USUARI
            usuariClass.InsertarUsuari(usuariClass);
            usuariClass.InsertarUsuari(usuariClass2);
        }

        [TestMethod]
        public void list(Usuaris usuariClass)
        {
            //LLISTAR USUARIS
            usuariClass.llistarUsuari();
        }

        [TestMethod]
        public void consultaUsuari(Usuaris usuariClass)
        {
            //CONSULTAR USUARIS
            usuariClass.consultarUsuari(usuariClass.nick);
        }

        [TestMethod]
        public void consultaContrassenya(String contrassenya, String nick)
        {
            //CONSULTAR CONTRASSENYA
            consultaContrassenya(contrassenya, nick);
        }

        [TestMethod]
        public void actualitza(Usuaris usuariClass2)
        {
            //ACTUALITZAR USUARI
            usuariClass2.actualitzarUsuari(usuariClass2);
        }

        [TestMethod]
        public void elimina(Usuaris usuariClass)
        {
            //ELIMINAR USUARI
            usuariClass.eliminarUsuari(usuariClass.nick);
        }

    }
}
