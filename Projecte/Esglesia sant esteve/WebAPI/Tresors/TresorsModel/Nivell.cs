﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace TresorsModel.Models
{
    public class Nivell
    {
        public ObjectId _id { get; set; }
        public int id_nivell { get; set; }
        public string nom { get; set; }
        public string id_minijoc { get; set; }
        public int id_personatge { get; set; }

        public IMongoDatabase connectar()
        {
            IMongoClient _client = new MongoClient("mongodb://localhost");
            IMongoDatabase _database = _client.GetDatabase("Tresors");
            return _database;
        }

        public void InsertarNivell(Nivell nivell)
        {
            IMongoDatabase db = connectar();

            IMongoCollection<Nivell> collection = db.GetCollection<Nivell>("NIVELL");
            Task task = collection.InsertOneAsync(nivell);
            task.Wait();
        }

        public IEnumerable<Nivell> llistarNivells()
        {
            IMongoDatabase db = connectar();

            IMongoCollection<Nivell> collection = db.GetCollection<Nivell>("NIVELL");
            IEnumerable<Nivell> llistaNivells = (from data in collection.AsQueryable() select data).ToList();
            return llistaNivells;
        }

        public Nivell consultarNivell(int id_nivell)
        {
            IMongoDatabase db = connectar();

            IMongoCollection<Nivell> collection = db.GetCollection<Nivell>("NIVELL");
            Nivell documentNivell = (from data in collection.AsQueryable<Nivell>()
                                 where data.id_nivell.Equals(id_nivell)
                                 select data).FirstOrDefault();
            return documentNivell;
        }

        public Boolean actualitzarNivell(Nivell nivell)
        {
            IMongoDatabase db = connectar();

            Nivell documentNivell = consultarNivell(id_nivell);
            IMongoCollection<Nivell> collection = db.GetCollection<Nivell>("NIVELL");
            var filtrarIdNivell = Builders<Nivell>.Filter.Eq("id_nivell", documentNivell.id_nivell);
            var actualitza = collection.ReplaceOneAsync(filtrarIdNivell, documentNivell);
            return true;
        }


        public Boolean eliminarNivell(int id_nivell)
        {
            IMongoDatabase db = connectar();

            Nivell documentNivell = consultarNivell(id_nivell);
            IMongoCollection<Nivell> collection = db.GetCollection<Nivell>("NIVELL");

            var filtrarIdNivell = Builders<Nivell>.Filter.Eq("id_nivell", documentNivell.id_nivell);
            var elimina = collection.DeleteManyAsync(filtrarIdNivell).Result;
            return true;
        }
    }
}