﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace TresorsModel.Models
{
    public class Personatge
    {
        public ObjectId _id { get; set; }
        public int id_personatge { get; set; }
        public string nom { get; set; }
        public string sprite { get; set; }
        public int id_pregunta { get; set; }

        public IMongoDatabase connectar()
        {
            IMongoClient _client = new MongoClient("mongodb://localhost");
            IMongoDatabase _database = _client.GetDatabase("Tresors");
            return _database;
        }

        public void InsertarPersonatge(Personatge personatge)
        {
            IMongoDatabase db = connectar();

            IMongoCollection<Personatge> collection = db.GetCollection<Personatge>("PERSONATGE");
            Task task = collection.InsertOneAsync(personatge);
            task.Wait();
        }

        public IEnumerable<Personatge> llistarPersonatge()
        {
            IMongoDatabase db = connectar();

            IMongoCollection<Personatge> collection = db.GetCollection<Personatge>("PERSONATGE");
            IEnumerable<Personatge> llistaPersonatge = (from data in collection.AsQueryable() select data).ToList();
            return llistaPersonatge;
        }

        public Personatge consultarPersonatge(int id_personatge)
        {
            IMongoDatabase db = connectar();

            IMongoCollection<Personatge> collection = db.GetCollection<Personatge>("PERSONATGE");
            Personatge documentPersonatge = (from data in collection.AsQueryable<Personatge>()
                                     where data.id_personatge.Equals(id_personatge)
                                     select data).FirstOrDefault();
            return documentPersonatge;
        }

        public Boolean actualitzarPersonatge(Personatge personatge)
        {
            IMongoDatabase db = connectar();

            Personatge documentPersonatge = consultarPersonatge(id_personatge);
            IMongoCollection<Personatge> collection = db.GetCollection<Personatge>("PERSONATGE");
            var filtrarIdPersonatge = Builders<Personatge>.Filter.Eq("id_nivell", documentPersonatge.id_personatge);
            var actualitza = collection.ReplaceOneAsync(filtrarIdPersonatge, documentPersonatge);
            return true;
        }


        public Boolean eliminarPersonatge(int id_personatge)
        {
            IMongoDatabase db = connectar();

            Personatge documentPersonatge = consultarPersonatge(id_personatge);
            IMongoCollection<Personatge> collection = db.GetCollection<Personatge>("PERSONATGE");

            var filtrarIdPersonatge = Builders<Personatge>.Filter.Eq("id_nivell", documentPersonatge.id_personatge);
            var elimina = collection.DeleteManyAsync(filtrarIdPersonatge).Result;
            return true;
        }
    }
}
