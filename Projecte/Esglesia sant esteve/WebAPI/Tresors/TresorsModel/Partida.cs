﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace TresorsModel.Models
{
    public class Partida
    {
        public ObjectId _id { get; set; }
        public int id_partida { get; set; }
        public int puntuacio { get; set; }
        public string nick_usuari { get; set; }
        public int id_nivell { get; set; }

        public IMongoDatabase connectar()
        {
            IMongoClient _client = new MongoClient("mongodb://localhost");
            IMongoDatabase _database = _client.GetDatabase("Tresors");
            return _database;
        }

        public void InsertarPartida(Partida partida)
        {
            IMongoDatabase db = connectar();

            IMongoCollection<Partida> collection = db.GetCollection<Partida>("PARTIDA");
            Task task = collection.InsertOneAsync(partida);
            task.Wait();
        }

        public IEnumerable<Partida> llistarPartida()
        {
            IMongoDatabase db = connectar();

            IMongoCollection<Partida> collection = db.GetCollection<Partida>("PARTIDA");
            IEnumerable<Partida> llistaPartida = (from data in collection.AsQueryable() select data).ToList();
            return llistaPartida;
        }
        public Partida consultarPartida(int id_partida)
        {
            IMongoDatabase db = connectar();

            IMongoCollection<Partida> collection = db.GetCollection<Partida>("PARTIDA");
            Partida documentPartida = (from data in collection.AsQueryable<Partida>()
                                       where data.id_partida.Equals(id_partida)
                                       select data).FirstOrDefault();
            return documentPartida;
        }

        public Boolean actualitzarPartida(Partida partida)
        {
            IMongoDatabase db = connectar();

            Partida documentPartida = consultarPartida(id_partida);
            IMongoCollection<Partida> collection = db.GetCollection<Partida>("PARTIDA");
            var filtraridPartida = Builders<Partida>.Filter.Eq("id_partida", documentPartida.id_partida);
            var actualitza = collection.ReplaceOneAsync(filtraridPartida, documentPartida);
            return true;
        }

        public Boolean eliminarPartida(int id_partida)
        {
            IMongoDatabase db = connectar();

            Partida documentPartida = consultarPartida(id_partida);
            IMongoCollection<Partida> collection = db.GetCollection<Partida>("PARTIDA");

            var filtrarIdPartida = Builders<Partida>.Filter.Eq("id_partida", documentPartida.id_partida);
            var elimina = collection.DeleteManyAsync(filtrarIdPartida).Result;
            return true;
        }
    }
}
