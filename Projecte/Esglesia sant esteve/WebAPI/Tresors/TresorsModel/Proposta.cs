﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace TresorsModel.Models
{
    public class Proposta
    {
        public ObjectId _id { get; set; }
        public int id_proposta { get; set; }
        public bool correcte { get; set; }
        public string text_proposta1 { get; set; }
        public string text_proposta2 { get; set; }
        public string text_proposta3 { get; set; }
        public int id_pregunta { get; set; }

        public IMongoDatabase connectar()
        {
            IMongoClient _client = new MongoClient("mongodb://localhost");
            IMongoDatabase _database = _client.GetDatabase("Tresors");
            return _database;
        }
        public void InsertarProposta(Proposta proposta)
        {
            IMongoDatabase db = connectar();

            IMongoCollection<Proposta> collection = db.GetCollection<Proposta>("PROPOSTA");
            Task task = collection.InsertOneAsync(proposta);
            task.Wait();
        }

        public IEnumerable<Proposta> llistarProposta()
        {
            IMongoDatabase db = connectar();

            IMongoCollection<Proposta> collection = db.GetCollection<Proposta>("PROPOSTA");
            IEnumerable<Proposta> llistaProposta = (from data in collection.AsQueryable() select data).ToList();
            return llistaProposta;
        }
        public Proposta consultarProposta(int id_proposta)
        {
            IMongoDatabase db = connectar();

            IMongoCollection<Proposta> collection = db.GetCollection<Proposta>("PROPOSTA");
            Proposta documentProposta = (from data in collection.AsQueryable<Proposta>()
                                       where data.id_proposta.Equals(id_proposta)
                                       select data).FirstOrDefault();
            return documentProposta;
        }

        public Boolean actualitzarProposta(Proposta proposta)
        {
            IMongoDatabase db = connectar();

            Proposta documentProposta = consultarProposta(id_proposta);
            IMongoCollection<Proposta> collection = db.GetCollection<Proposta>("PROPOSTA");
            var filtraridProposta = Builders<Proposta>.Filter.Eq("id_proposta", documentProposta.id_proposta);
            var actualitza = collection.ReplaceOneAsync(filtraridProposta, documentProposta);
            return true;
        }

        public Boolean eliminarProposta(int id_proposta)
        {
            IMongoDatabase db = connectar();

            Proposta documentProposta = consultarProposta(id_proposta);
            IMongoCollection<Proposta> collection = db.GetCollection<Proposta>("PROPOSTA");

            var filtrarIdProposta = Builders<Proposta>.Filter.Eq("id_proposta", documentProposta.id_proposta);
            var elimina = collection.DeleteManyAsync(filtrarIdProposta).Result;
            return true;
        }
    }
}
