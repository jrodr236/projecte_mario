﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace TresorsModel.Models
{
    public class Usuaris
    {
        public ObjectId _id { get; set; }
        public string uuid { get; set; }
        public string nick { get; set; }
        public string nom { get; set; }
        public string cognom { get; set; }
        public string contrassenya { get; set; }
        public string email { get; set; }
        public int nivell { get; set; }

        public IMongoDatabase connectar()
        {
            IMongoClient _client = new MongoClient("mongodb://localhost");
            IMongoDatabase _database = _client.GetDatabase("Tresors");
            return _database;
        }

        public void InsertarUsuari(Usuaris usuari)
        {
            IMongoDatabase db = connectar();

            IMongoCollection<Usuaris> collection = db.GetCollection<Usuaris>("USUARI");
            usuari.nivell = 1;
            Task task = collection.InsertOneAsync(usuari);
            task.Wait();
        }

        public IEnumerable<Usuaris> llistarUsuari()
        {
            IMongoDatabase db = connectar();
            
            IMongoCollection<Usuaris> collection = db.GetCollection<Usuaris>("USUARI");
            IEnumerable<Usuaris> llistaUsuari = (from data in collection.AsQueryable() select data).ToList();
            return llistaUsuari;
        }


        public Usuaris consultarUsuari(String nick)
        {
            IMongoDatabase db = connectar();

            IMongoCollection<Usuaris> collection = db.GetCollection<Usuaris>("USUARI");
            Usuaris documentUsuari = (from data in collection.AsQueryable<Usuaris>()
                                        where data.nick.Equals(nick)
                                        select data).FirstOrDefault();
            return documentUsuari;            
        }

        public Usuaris consultarCorreu(String email)
        {
            IMongoDatabase db = connectar();

            IMongoCollection<Usuaris> collection = db.GetCollection<Usuaris>("USUARI");
            Usuaris documentUsuari = (from data in collection.AsQueryable<Usuaris>()
                                      where data.email.Equals(email)
                                      select data).FirstOrDefault();
            return documentUsuari;
        }

        public Usuaris consultarUuid(String uuid)
        {
            IMongoDatabase db = connectar();

            IMongoCollection<Usuaris> collection = db.GetCollection<Usuaris>("USUARI");
            Usuaris documentUsuari = (from data in collection.AsQueryable<Usuaris>()
                                      where data.uuid.Equals(uuid)
                                      select data).FirstOrDefault();
            return documentUsuari;
        }


        public Usuaris consultaContrassenya(String contrassenya, String nick)
        {
            IMongoDatabase db = connectar();
            
            IMongoCollection<Usuaris> collection = db.GetCollection<Usuaris>("USUARI");
            Usuaris documentUsuari = (from data in collection.AsQueryable<Usuaris>()
                                      where data.nick.Equals(nick) && data.contrassenya.Equals(contrassenya)
                                      select data).FirstOrDefault();
            return documentUsuari;
        }

        
        public Boolean actualitzarUsuari(Usuaris usuari)
        {
            IMongoDatabase db = connectar();

            Usuaris documentUsuari = consultarUsuari(nick);
            IMongoCollection<Usuaris> collection = db.GetCollection<Usuaris>("USUARI");
            var filtrarNick = Builders<Usuaris>.Filter.Eq("nick", usuari.nick);
            var actualitza = collection.ReplaceOneAsync(filtrarNick, usuari);
            return true;
        }

        public Boolean eliminarUsuari(String nick)
        {
            IMongoDatabase db = connectar();

            Usuaris documentUsuari = consultarUsuari(nick);         
            IMongoCollection<Usuaris> usuari = db.GetCollection<Usuaris>("USUARI");

            var filtrarNick = Builders<Usuaris>.Filter.Eq("nick", documentUsuari.nick);
            var elimina = usuari.DeleteManyAsync(filtrarNick).Result;
            return true;
        }
    }      
}

