﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace TresorsModel.Models{
    public class MiniJoc
    {
        public ObjectId _id { get; set; }
        public int id_joc { get; set; }
        public int puntuacio_max { get; set; }
        public string titol { get; set; }

        public IMongoDatabase connectar()
        {
            IMongoClient _client = new MongoClient("mongodb://localhost");
            IMongoDatabase _database = _client.GetDatabase("Tresors");
            return _database;
        }

        public void InsertarMiniJoc(MiniJoc miniJoc)
        {
            IMongoDatabase db = connectar();

            IMongoCollection<MiniJoc> collection = db.GetCollection<MiniJoc>("MINIJOC");
            Task task = collection.InsertOneAsync(miniJoc);
            task.Wait();
        }
        public IEnumerable<MiniJoc> llistarMiniJoc()
        {
            IMongoDatabase db = connectar();

            IMongoCollection<MiniJoc> miniJoc = db.GetCollection<MiniJoc>("MINIJOC");
            IEnumerable<MiniJoc> llistaMiniJoc = (from data in miniJoc.AsQueryable() select data).ToList();
            return llistaMiniJoc;
        }

        public MiniJoc consultarMiniJoc(int id_miniJoc)
        {
            IMongoDatabase db = connectar();

            IMongoCollection<MiniJoc> collection = db.GetCollection<MiniJoc>("MINIJOC");
            MiniJoc documentMiniJoc = (from data in collection.AsQueryable<MiniJoc>()
                                      where data.id_joc.Equals(id_miniJoc)
                                      select data).FirstOrDefault();
            return documentMiniJoc;
        }

        public Boolean actualitzarMiniJoc(MiniJoc miniJoc)
        {
            IMongoDatabase db = connectar();

            MiniJoc documentMiniJoc = consultarMiniJoc(id_joc);
            IMongoCollection<MiniJoc> collection = db.GetCollection<MiniJoc>("MINIJOC");
            var filtraridJoc = Builders<MiniJoc>.Filter.Eq("id_joc", documentMiniJoc.id_joc);
            var actualitza = collection.ReplaceOneAsync(filtraridJoc, documentMiniJoc);
            return true;
        }

        public Boolean eliminarMiniJoc(int id_joc)
        {
            IMongoDatabase db = connectar();

            MiniJoc documentMiniJoc = consultarMiniJoc(id_joc);
            IMongoCollection<MiniJoc> collection = db.GetCollection<MiniJoc>("MINIJOC");

            var filtrarIdJoc = Builders<MiniJoc>.Filter.Eq("id_joc", documentMiniJoc.id_joc);
            var elimina = collection.DeleteManyAsync(filtrarIdJoc).Result;
            return true;
        }
    }
}

