﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace TresorsModel.Models
{
    public class Pregunta
    {
        public ObjectId _id { get; set; }
        public int id_nivell { get; set; }
        public int id_pregunta { get; set; }
        public string textPregunta { get; set; }
        public string textResposta1 { get; set; }
        public string textResposta2 { get; set; }
        public string textResposta3 { get; set; }
        public string textResposta4 { get; set; }
        public int correcte { get; set; }

        public IMongoDatabase connectar()
        {
            IMongoClient _client = new MongoClient("mongodb://localhost");
            IMongoDatabase _database = _client.GetDatabase("Tresors");
            return _database;
        }

        public void InsertarPregunta(Pregunta pregunta)
        {
            IMongoDatabase db = connectar();

            IMongoCollection<Pregunta> collection = db.GetCollection<Pregunta>("PREGUNTA");
            Task task = collection.InsertOneAsync(pregunta);
            task.Wait();
        }

        public IEnumerable<Pregunta> llistarPregunta()
        {
            IMongoDatabase db = connectar();

            IMongoCollection<Pregunta> collection = db.GetCollection<Pregunta>("PREGUNTA");
            IEnumerable<Pregunta> llistaPregunta = (from data in collection.AsQueryable() select data).ToList();
            return llistaPregunta;
        }

        public IEnumerable<Pregunta> llistarPreguntes(int id_nivell)
        {
            IMongoDatabase db = connectar();

            IMongoCollection<Pregunta> collection = db.GetCollection<Pregunta>("PREGUNTA");
            IEnumerable<Pregunta> llistaPregunta = (from data in collection.AsQueryable<Pregunta>()
                                                    where data.id_nivell.Equals(id_nivell)
                                                    select data).ToList();
            return llistaPregunta;
        }

        public Pregunta consultarPregunta(int id_nivell)
        {
            IMongoDatabase db = connectar();

            IMongoCollection<Pregunta> collection = db.GetCollection<Pregunta>("PREGUNTA");
            Pregunta documentPregunta = (from data in collection.AsQueryable<Pregunta>()
                                       where data.id_nivell.Equals(id_nivell)
                                       select data).FirstOrDefault();
            return documentPregunta;
        }

        public Boolean actualitzarPregunta(Pregunta pregunta)
        {
            IMongoDatabase db = connectar();

            Pregunta documentPregunta = consultarPregunta(id_pregunta);
            IMongoCollection<Pregunta> collection = db.GetCollection<Pregunta>("PREGUNTA");
            var filtraridPregunta = Builders<Pregunta>.Filter.Eq("id_pregunta", documentPregunta.id_pregunta);
            var actualitza = collection.ReplaceOneAsync(filtraridPregunta, documentPregunta);
            return true;
        }

        public Boolean eliminarPregunta(int id_pregunta)
        {
            IMongoDatabase db = connectar();

            Pregunta documentPregunta = consultarPregunta(id_pregunta);
            IMongoCollection<Pregunta> collection = db.GetCollection<Pregunta>("PREGUNTA");

            var filtrarIdPregunta = Builders<Pregunta>.Filter.Eq("id_pregunta", documentPregunta.id_pregunta);
            var elimina = collection.DeleteManyAsync(filtrarIdPregunta).Result;
            return true;
        }
    }
}