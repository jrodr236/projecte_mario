﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace TresorsModel.Models
{
    public class Lloc
    {
        public ObjectId _id { get; set; }
        public int id_lloc { get; set; }
        public string nom { get; set; }
        public string fons1 { get; set; }
        public string fons2 { get; set; }
        public string fons3 { get; set; }
        public int id_nivell { get; set; }

        public IMongoDatabase connectar()
        {
            IMongoClient _client = new MongoClient("mongodb://localhost");
            IMongoDatabase _database = _client.GetDatabase("Tresors");
            return _database;
        }

        public void InsertarLloc(Lloc lloc)
        {
            IMongoDatabase db = connectar();

            IMongoCollection<Lloc> collection = db.GetCollection<Lloc>("LLOC");
            Task task = collection.InsertOneAsync(lloc);
            task.Wait();
        }

        public IEnumerable<Lloc> llistarLlocs()
        {
            IMongoDatabase db = connectar();

            IMongoCollection<Lloc> collection = db.GetCollection<Lloc>("LLOC");
            IEnumerable<Lloc> llistaLlocs = (from data in collection.AsQueryable() select data).ToList();
            return llistaLlocs;
        }

        public Lloc consultarLloc(int id_lloc)
        {
            IMongoDatabase db = connectar();

            IMongoCollection<Lloc> collection = db.GetCollection<Lloc>("LLOC");
            Lloc documentLloc = (from data in collection.AsQueryable<Lloc>()
                                      where data.id_lloc.Equals(id_lloc)
                                      select data).FirstOrDefault();
            return documentLloc;
        }

        public Boolean actualitzarLloc(Lloc lloc)
        {
            IMongoDatabase db = connectar();

            Lloc documentLloc = consultarLloc(id_lloc);
            IMongoCollection<Lloc> collection = db.GetCollection<Lloc>("LLOC");
            var filtrarIdLloc = Builders<Lloc>.Filter.Eq("id_lloc", documentLloc.id_lloc);
            var actualitza = collection.ReplaceOneAsync(filtrarIdLloc, documentLloc);
            return true;
        }


        public Boolean eliminarLloc(int id_lloc)
        {
            IMongoDatabase db = connectar();

            Lloc documentLloc = consultarLloc(id_lloc);
            IMongoCollection<Lloc> collection = db.GetCollection<Lloc>("LLOC");

            var filtrarIdLloc = Builders<Lloc>.Filter.Eq("id_lloc", documentLloc.id_lloc);
            var elimina = collection.DeleteManyAsync(filtrarIdLloc).Result;
            return true;
        }
    }
}
